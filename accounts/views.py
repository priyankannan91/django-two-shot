from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm, SignupForm
from django.contrib.auth.models import User


# Create your views here.
def login_view(request):
    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            # calling username field from .forms
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            # confirm if username+password match database
            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                print("test")
                # pulls home from project URL
                return redirect("home")
            else:
                form.add_error("username", "Invalid Login")
    else:
        form = LoginForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("login")


def signup_view(request):
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            # data is stored in a dictionary for retrevial
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]
            if password == password_confirmation:
                # create a new login and save info to properties
                user = User.objects.create_user(
                    # ?? Why isn't it username=username?
                    # Excpects it in order of username, then password
                    # Else you need to manually set it ex. password=username; username=password
                    username,
                    password,
                )
                # login user
                login(request, user)
                # redirect to home page
                return redirect("home")
            else:
                form.add_error("password", "Password do not match")
    else:
        form = SignupForm()
    context = {
        "form": form,
    }
    return render(request, "accounts/signup.html", context)
