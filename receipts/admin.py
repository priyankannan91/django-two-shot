from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt

# QUESTION: Feature 3 Test:
# Creating test database for alias 'default'...
# "Destroying test database for alias 'default'..."


# Register your models here.
@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "id",
    )


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
        "id",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "category",
        "account",
        "purchaser",
        "id",
    )
